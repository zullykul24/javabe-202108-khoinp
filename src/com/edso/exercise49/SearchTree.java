package com.edso.exercise49;

public class SearchTree implements NodeList {
    ListItem root = null;

    public SearchTree(ListItem root){
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return this.root;
    }

    @Override
    public boolean addItem(ListItem item) {
        if(this.root == null){
            this.root = item;
            return true;
        }
        ListItem currentItem = this.root;
        while (currentItem != null){
            int comparison = currentItem.compareTo(item);
            if(comparison < 0){
                //currentItem has smaller value
                //move right
                if(currentItem.next() != null){
                    currentItem = currentItem.next();
                } else {
                    currentItem.setNext(item);
                    return true;
                }
            } else if(comparison > 0){
                //currentItem has a greater value
                //move left
                if(currentItem.previous() != null){
                    currentItem = currentItem.previous();
                } else {
                    currentItem.setPrevious(item);
                    return true;
                }
            } else {
                //equal
                //dont add
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        ListItem currentItem = this.root;
        ListItem parentItem = currentItem;

        while (currentItem != null){
            int comparison = currentItem.compareTo(item);
            if(comparison < 0){
                //currentItem has a smaller value
                //move right
                parentItem = currentItem;
                currentItem = currentItem.next();
            } else if(comparison > 0){
                //currentItem has a greater value
                //move left
                parentItem = currentItem;
                currentItem = currentItem.previous();
            } else {
                //found the item
                this.performRemoval(currentItem, parentItem);
            }
        }

        return false;
    }

    @Override
    public void traverse(ListItem root) {
        if (root != null){
            traverse(root.previous());
            System.out.println(root.getValue());
            traverse(root.next());
        }
    }

    private void performRemoval(ListItem item, ListItem parentItem){
        if(item.next() == null){
            // no right tree, make parent point to left tree
            if(parentItem.next() == item){
                parentItem.setNext(item.previous());
            } else if (parentItem.previous() == item){
                parentItem.setPrevious(item.previous());
            } else {
                // parent is item
                //at top of the tree
                root = item.previous();
            }
        } else if(item.previous() == null){
            //no left tree, make parent point to right tree
            if(parentItem.next() == item){
                parentItem.setNext(item.next());
            } else if(parentItem.previous() == item){
                parentItem.setPrevious(item.next());
            } else {
                // parent is item
                //at top of the tree
                root = item.next();
            }
        } else {
            // From the right sub-tree, find the smallest value (i.e., the leftmost).
            ListItem current = item.next();
            ListItem leftMostParent = item;
            while (current.previous() != null) {
                leftMostParent = current;
                current = current.previous();
            }
            // Now put the smallest value into our node to be deleted
            item.setValue(current.getValue());
            // and delete the smallest
            if (leftMostParent == item) {
                // there was no leftmost node, so 'current' points to the smallest
                // node (the one that must now be deleted).
                item.setNext(current.next());
            } else {
                // set the smallest node's parent to point to
                // the smallest node's right child (which may be null).
                leftMostParent.setPrevious(current.next());
            }

        }
    }

    public static void main(String[] args) {
        SearchTree tree = new SearchTree(new Node("30"));
        tree.addItem(new Node("20"));
        tree.traverse(tree.getRoot());
        System.out.println("Let's add more Nodes");
        tree.addItem(new Node("31"));
        tree.addItem(new Node("26"));
        tree.addItem(new Node("21"));
        tree.addItem(new Node("1"));
        tree.addItem(new Node("42"));
        tree.traverse(tree.getRoot());
        System.out.println("Let's remove the root");
        tree.removeItem(new Node("30"));
        tree.traverse(tree.getRoot());
    }
}
