package com.edso.bai4tuan3_client_server.client;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class MessagingClient {
    public static void main(String[] args) throws IOException {
        try {
            Socket socket = new Socket("localhost", 5000);
            new Thread(new GetMessageThread(socket)).start();
            new Thread(new SendMessageThread(socket)).start();
        } catch (ConnectException e){
            System.out.println("Server is not running");
        }
    }
}
