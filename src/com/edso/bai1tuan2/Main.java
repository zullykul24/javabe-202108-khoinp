package com.edso.bai1tuan2;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
       // String pattern = "/^(tail -)\\d+ (\\w+|.)+\\w+/gm";
        try{
            scanner.skip(Pattern.compile("tail -"));
            while (true){
                int numOfLines = scanner.nextInt();
                String filePath = scanner.next();
                if(numOfLines <= 0){
                    System.out.println("Số dòng cần lấy phải > 0. Hãy nhập lại");
                    continue;
                } else {
                    Tail.readLastLines(numOfLines, filePath);
                    break;
                }
            }

        }catch (NoSuchElementException e){
            System.out.println("Nhập sai cú pháp");
        }
    }

}
