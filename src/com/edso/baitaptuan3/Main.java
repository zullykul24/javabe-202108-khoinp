package com.edso.baitaptuan3;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Timer;

import static com.edso.baitaptuan3.Main.*;

public class Main {
    public static Messenger messenger;
    public static final int MAX_LENGTH = 5;
    public static int numOfEnqueue = 0;
    public static int numOfDequeue = 0;
    public static final int PRODUCER_DELAY = 3000;
    public static final int CONSUMER_DELAY = 5000;
    private static final int COUNT_ENQUEUE_DEQUEUE_PERIOD = 10000;
    private static final int QUEUE_SIZE_CHECK_PERIOD = 3000;

    public static void main(String[] args) {
        //After each 10 seconds, print out numOfEnqueue and numOfDequeue
        CountTask countTask = new CountTask();
        Timer timer = new Timer();
        timer.schedule(countTask, 0, COUNT_ENQUEUE_DEQUEUE_PERIOD);

        //After each 3 seconds, print out queue size
        QueueSizeCheckTask queueSizeCheckTask = new QueueSizeCheckTask();
        Timer checkSizeTimer = new Timer();
        checkSizeTimer.schedule(queueSizeCheckTask, 0, QUEUE_SIZE_CHECK_PERIOD);

        messenger = new Messenger();
        (new Thread(new Producer(messenger, ThreadColor.ANSI_RED))).start();
        (new Thread(new Producer(messenger, ThreadColor.ANSI_CYAN))).start();
        (new Thread(new Consumer(messenger, ThreadColor.ANSI_BLUE))).start();
        (new Thread(new Consumer(messenger, ThreadColor.ANSI_GREEN))).start();
    }
}

class Message {
    private int id;
    private String content;

    public Message(int id, String content) {
        this.id = id;
        this.content = content;
    }


    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", content='" + content + '\'' +
                '}';
    }
}

class Messenger{
    private Queue<Message> messageQueue = new LinkedList<>();
    private static Integer i = 0;

    public int size(){
        return messageQueue.size();
    }

    public synchronized String read(String color){
        while (messageQueue.isEmpty()){
            try {
                System.out.println(ThreadColor.ANSI_RESET + "Queue is empty");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String msg = messageQueue.poll().toString();
        if(msg != null){
            System.out.println(color + msg + " WAS READ");
        }
        Main.numOfDequeue++;
        notifyAll();
        return msg;
    }

    public synchronized void write(String message, String color){
        while (messageQueue.size() == MAX_LENGTH){
            try {
                System.out.println(ThreadColor.ANSI_RESET + "Queue is full");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Message m = new Message(i++, message);
        if(messageQueue.offer(m)){
            System.out.println(color + m.toString()+ " was written");
            Main.numOfEnqueue++;
        }
        notifyAll();
    }

}

class Producer implements Runnable{
    private Messenger messenger;
    String color;

    public Producer(Messenger messenger, String color) {
        this.messenger = messenger;
        this.color = color;
    }

    @Override
    public void run() {
        while (true) {
            Random random = new Random();
            String newMessage =  "new msg: " + random.nextInt();
            messenger.write(newMessage, color);
            try {
                Thread.sleep(PRODUCER_DELAY);
            } catch (InterruptedException e) {

            }
        }
    }
}



class Consumer implements Runnable{
    private Messenger messenger;
    private String color;

    public Consumer(Messenger messenger, String color) {
        this.messenger = messenger;
        this.color = color;
    }

    @Override
    public void run() {
        while (true) {
            String readMessage = messenger.read(color);
            try {
                Thread.sleep(CONSUMER_DELAY);
            } catch (InterruptedException e) {

            }
        }
    }
}