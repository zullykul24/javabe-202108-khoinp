package com.edso.bai1_random_double_array;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class RandomDoubleArray {
    private ArrayList<Double> doubleArrayList;

    //generate a new ArrayList to store those numbers left
    private ArrayList<Double> arrayNumbersLeft;

    //generate a new ArrayList to store numbers smaller than "number"
    private ArrayList<Double> arrayNumbersDeleted;


    public void generateArray(int n){
        this.doubleArrayList = new ArrayList<>();
        for(int i = 0; i<n;i++){
            double random = new Random().nextDouble();
            this.doubleArrayList.add(Double.valueOf(random) * 100);
            System.out.println(this.doubleArrayList.get(i));
        }
    }
    public void deleteValueSmallerThan(int number){
        this.arrayNumbersLeft = new ArrayList<>();
        this.arrayNumbersDeleted = new ArrayList<>();
        for(Double i:this.doubleArrayList){
            if(i < number){
                this.arrayNumbersDeleted.add(i);
            } else {
                this.arrayNumbersLeft.add(i);
            }
        }

        /// replace the old array with arrayNumberLeft;
        this.doubleArrayList.clear();
        for(Double i : arrayNumbersLeft){
            doubleArrayList.add(i);
        }
    }

    public void printArray(){
        System.out.println("Array:");
        for(Double i : this.doubleArrayList){
            System.out.println(i + "");
        }
    }

    public void printDeletedArray(){
        System.out.println("Deleted array:");
        for(Double i : this.arrayNumbersDeleted){
            System.out.println(i + "");
        }
    }

    public static void main(String[] args) {
        RandomDoubleArray instance = new RandomDoubleArray();
        System.out.print("Enter number of double values to be generated:");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        //generate array
        instance.generateArray(n);

        System.out.print("Enter the number that you want to deleted all the numbers smaller than it:");
        Scanner scanner2 = new Scanner(System.in);
        int m = scanner.nextInt();

        //delete values smaller than m
        instance.deleteValueSmallerThan(m);

        //print new arr
        instance.printArray();

        //print deleted arr
        instance.printDeletedArray();
    }
}
