package com.edso.exercise50;

public class Config {
    public static final int QUIT_LOCATION = 0;
    public static final int END_OF_ROAD_LOCATION = 1;
    public static final int TOP_OF_HILL_LOCATION = 2;
    public static final int BUILDING_LOCATION = 3;
    public static final int VALLEY_LOCATION = 4;
    public static final int FOREST_LOCATION = 5;
}
