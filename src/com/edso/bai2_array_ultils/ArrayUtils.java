package com.edso.bai2_array_ultils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayUtils<T extends Object> {
    private static ArrayUtils instance;     //lazy initialized singleton

    private ArrayUtils(){}

    public static ArrayUtils getInstance() {
        if(instance == null){
            instance = new ArrayUtils();
        }
        return instance;
    }

    public void display(List<T> list){
        if(list == null){
            System.out.println("List is null. Please try again");
            return;
        }
        if(list.isEmpty()){
            System.out.println("List is empty. Please try again");
            return;
        }
        for(T t : list){
            if(t == null){
                System.out.println("List has a null element");
                return;
            }
            System.out.println(t);
        }
    }

    public static void main(String[] args) {
        List<Integer> arrayInt = Arrays.asList(1,2,3,4,5,6,7,8,9);
        List<Integer> nullArr = null;
        List<Integer> elementNullArr = Arrays.asList(null,2,3,4,5,6,7,8,9);
        ArrayUtils.getInstance().display(nullArr);
    }
}
