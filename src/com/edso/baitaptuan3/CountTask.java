package com.edso.baitaptuan3;

import java.util.TimerTask;

public class CountTask extends TimerTask {
    @Override
    public void run() {
        System.out.println(ThreadColor.ANSI_RESET + "----------------------numOfEnqueueInThisPeriod = " + Main.numOfEnqueue);
        System.out.println(ThreadColor.ANSI_RESET + "----------------------numOfDequeueInThisPeriod = " +Main.numOfDequeue);

        Main.numOfDequeue = 0;
        Main.numOfEnqueue = 0;
    }
}
