package com.edso.exercise48;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    // write code here
    private String name;
    private String artist;
    private SongList songs;

    public Album(String name, String artist){
        this.name = name;
        this.artist = artist;
        this.songs = new SongList();
    }

    public boolean addSong(String title, double duration){
        if(this.songs.findSong(title) == null){
            this.songs.add(new Song(title, duration));
            return true;
        }
        return false;
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> linkedListSongs){
        Song foundSong = this.songs.findSong(trackNumber);
        if(foundSong != null){
            linkedListSongs.add(foundSong);
            return true;
        } else {
            System.out.println("Track number " + trackNumber + " was not found in album " + this.name);
        }
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> linkedListSongs){
        if(this.songs.findSong(title) != null){
            linkedListSongs.add(this.songs.findSong(title));
            return true;
        } else {
            System.out.println("Song named '" + title + "' was not found in album " + this.name);
        }
        return false;

    }
    public static class SongList {
        private ArrayList<Song> songs;

        private SongList(){
            this.songs = new ArrayList<>();
        }

        private boolean add(Song song){
            this.songs.add(song);
            return true;
        }

        private Song findSong(String title){
            for(Song i : this.songs){
                if(i.getTitle().equals(title)){
                    return i;
                }
            }
            return null;
        }

        private Song findSong(int trackNumber){
            if(trackNumber < 1 || trackNumber > this.songs.size()){
                // size 10(index 0-> 9)
                // track 1 -> 10
                return null;
            }
            return this.songs.get(trackNumber - 1);
        }
    }

}