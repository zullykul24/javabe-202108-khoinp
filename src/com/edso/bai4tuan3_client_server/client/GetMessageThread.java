package com.edso.bai4tuan3_client_server.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class GetMessageThread implements Runnable{
    private final Socket socket;

    public GetMessageThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        BufferedReader echoes;
        try {
            echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String response = echoes.readLine();
                System.out.println(response);
            }
        } catch (Exception e){
            System.out.println("Client is disconnected");
            System.out.println(e.getMessage());
        }

    }
}