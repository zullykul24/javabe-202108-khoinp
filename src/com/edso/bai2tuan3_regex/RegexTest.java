package com.edso.bai2tuan3_regex;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class RegexTest {

    @Test
    public void isVietNamePhoneNumberWhenBeginWith0() {
        assertTrue(Regex.isVietNamePhoneNumber("0123456789"));
    }

    @Test
    public void isVietNamePhoneNumberWhenBeginWith84() {
        assertTrue(Regex.isVietNamePhoneNumber("84123456789"));
    }

    @Test
    public void isVietNamePhoneNumberWhenBeginWithpPlus84() {
        assertTrue(Regex.isVietNamePhoneNumber("+84123456789"));
    }

    @Test
    public void isVietNamePhoneNumberWhenBeginWith0AndMoreThan10Digits() {
        assertFalse(Regex.isVietNamePhoneNumber("01234567897"));
    }

    @Test
    public void isVietNamePhoneNumberWhenContainsLetter() {
        assertFalse(Regex.isVietNamePhoneNumber("8412345678a"));
    }

    @Test
    public void isVietNamePhoneNumberWhenNotBeginByOor84() {
        assertFalse(Regex.isVietNamePhoneNumber("3123456789"));
    }

    @Test
    public void isEmail() {
        assertTrue(Regex.isEmail("zullykulphinh@gmail.com"));
    }

    @Test
    public void isEmailWhenContainsManyDots() {
        assertTrue(Regex.isEmail("zullykul.phinh@gmail.com"));
    }

    @Test
    public void isEmailWhenContainsSplash() {
        assertFalse(Regex.isEmail("zullykul/phinh@gmail.com"));
    }

    @Test
    public void isEmailWhenNotContainsAtSign() {
        assertFalse(Regex.isEmail("zullykulphinhgmail.com"));
    }

    @Test
    public void isEmailWhenContainsMoreThanOneAtSign() {
        assertFalse(Regex.isEmail("zullykul@phinh@gmail.com"));
    }
}