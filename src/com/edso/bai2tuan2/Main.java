package com.edso.bai2tuan2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static List<Integer> numbersList;

    public static void main(String[] args) throws IOException {
        System.out.println("Input form: filePath numberOfThreads");
        System.out.println("Ex: input.txt 4");
        Scanner scanner = new Scanner(System.in);
        String file = scanner.next();
        int numOfThreads = scanner.nextInt();
        performReadFileThenSortThenWrite(file, numOfThreads);
    }

    public static void performReadFileThenSortThenWrite(String filePath, int thread) throws IOException {
        //a sample of small file
        //numbersList = readNumbersFile("multi_thread_sort_sample.txt");
        numbersList = readNumbersFile(filePath);
        MultiThreadSorting.performSort(numbersList, thread,"output.txt");
    }

    public static ArrayList<Integer> readNumbersFile(String filePath) throws IOException {
        ArrayList<Integer> numbersInFile = new ArrayList<>();
        FileReader fileToRead;
        try {
            fileToRead = new FileReader(filePath);
            BufferedReader buffer = new BufferedReader(fileToRead);
            String numberInLine;
            while ((numberInLine = buffer.readLine()) != null) {
                String[] temp = numberInLine.split(" ");
                int[] listNumber = Arrays.stream(temp).mapToInt(Integer::parseInt).toArray();
                for (int i : listNumber) {
                    numbersInFile.add(i);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File isn't exists");
        } finally {
            return numbersInFile;
        }
//        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(filePath)))) {
//            while (scanner.hasNextLine()) {
//                int number = scanner.nextInt();
//                numbersInFile.add(number);
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            return numbersInFile;
//        }
    }
}