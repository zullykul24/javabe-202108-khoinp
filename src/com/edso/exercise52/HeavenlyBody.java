package com.edso.exercise52;
/*
    Only add/edit code where it is stated in the description.
*/


import java.util.HashSet;
import java.util.Set;

public abstract class HeavenlyBody {
    private final Key key;
    private final double orbitalPeriod;
    private final Set<HeavenlyBody> satellites;

    public enum BodyTypes{
        PLANET,
        DWARF_PLANET,
        MOON
    }

    public HeavenlyBody(String name, double orbitalPeriod, BodyTypes bodyType) {
        this.orbitalPeriod = orbitalPeriod;
        this.satellites = new HashSet<>();
        this.key = new Key(name, bodyType);

    }

    public double getOrbitalPeriod() {
        return this.orbitalPeriod;
    }

    public Key getKey() {
        return this.key;
    }

    public boolean addSatellite(HeavenlyBody body){
        return this.satellites.add(body);
    }

    public Set<HeavenlyBody> getSatellites(){
        return new HashSet<>(this.satellites);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HeavenlyBody that = (HeavenlyBody) o;

        return this.key.equals(that.key);
    }

    @Override
    public final int hashCode() {
        return this.key.hashCode();
    }

    public static Key makeKey(String name, BodyTypes bodyType){
        return new Key(name, bodyType);
    }

    @Override
    public String toString() {
        return this.key.getName() + ": " + this.key.getBodyType() + ", " + this.getOrbitalPeriod();
    }

    ///nested class
    public static final class Key{
        private String name;
        private BodyTypes bodyType;



        private Key(String name, BodyTypes bodyType) {
            this.name = name;
            this.bodyType = bodyType;
        }

        public String getName() {
            return name;
        }

        public BodyTypes getBodyType() {
            return bodyType;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (!name.equals(key.name)) return false;
            return bodyType == key.bodyType;
        }

        @Override
        public int hashCode() {
            int result = name.hashCode() + bodyType.hashCode() + 11;
            return result;
        }

        @Override
        public String toString() {
            return this.name + ": " + this.bodyType;
        }
    }
}