package com.edso.bai4tuan3_client_server.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Echoer extends Thread{
    private Socket socket;

    public Echoer(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        String name = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        BufferedReader input = null;
        PrintWriter output = null;
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            String outPrint;

            //request name from new client
            while (true) {
                output.println("Enter your name: ");
                //have to wait here
                name = input.readLine();
                if (name == null) {
                    return;
                }
                break;
            }
            outPrint = "[" + formatter.format(new Date()) + "] [" + name + "] joined the chat room";
            System.out.println(outPrint);


            MessagingServer.writers.add(output);

            //inform everyone that new client has joined the chat room
            for (PrintWriter i : MessagingServer.writers) {
                i.println(outPrint);
            }

            //get messages from clients and forward them
            while (true){
                try {
                    String message = input.readLine();
                    if(message.equals("exit")){
                        MessagingServer.writers.remove(output);
                        String quitMessage = "[" + formatter.format(new Date()) + "] [" + name + "] has quit this chat";
                        System.out.println(quitMessage);
                        for (PrintWriter i : MessagingServer.writers) {
                            i.println(quitMessage);
                        }
                        break;
                    }
                    String messageToClients = "[" + formatter.format(new Date()) + "] [" + name + "] " + message;
                    System.out.println(messageToClients);


                    for (PrintWriter i : MessagingServer.writers) {
                        i.println(messageToClients);
                    }


                } catch (SocketException e){
                    //when client close connection manually, not using "exit" command
                    MessagingServer.writers.remove(output);
                    String quitMessage = "[" + formatter.format(new Date()) + "] [" + name + "] has quit this chat";
                    System.out.println(quitMessage);
                    for (PrintWriter i : MessagingServer.writers) {
                        i.println(quitMessage);
                    }
                    break;
                }


            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e){
                //
            }
        }
    }
}